# flux-infra-test

Manifests used by FluxCD. Bootstrapping is done with the Terraform that
creates a secret for Git repository, installs FluxCD and creates
GitRepository and the main Kustomization named `flux-system`.

## Kustomization vs Kustomization

`Kustomization` means both a manifest for `kustomize` tool and a type of
application installed by FluxCD. To prevent confusion, all manifest files are
named always `kustomization.yaml` and are named here "manifests" then all
application files have the prefix `ks-`` and are named here
"(FluxCD) applications".

The manifest might be verified with:

```sh
kustomize build ./DIRECTORY
```

then piped into `kubeconform` or `kubectl apply --dry-run=client -f-`

## Applications installed by FluxCD

FluxCD applications are Kustomizations and HelmReleases.

The main applications are:

- flux-system (first bootstrapped by Terraform then managed as `flux-system` HelmRelease)
- infrastructure (then it installs other Kustomizations and HelmReleases)
- tenants (installs Kustomizations that create separate namespaces for the teams + other resources in these namespaces)

## Permissions

Because of multi-tenancy mode applications must be installed from
`flux-system` namespace and Kustomizations with
`serviceAccountName: kustomize-controller` and HelmReleases with
`serviceAccountName: helm-controller`.

## CRDs

All HelmReleases that bring Custom Resource Definitions must have:

```yaml
spec:
  install:
    crds: CreateReplace
  upgrade:
    crds: CreateReplace
```

## Remote vs local Helm charts

The HelmReleases can have `sourceRef` either `kind: HelmRepository` or
`kind: GitRepostiory` (they are remote or local).

The remote charts should have `reconcileStrategy: ChartVersion` and local
charts should have `reconcileStrategy: Revision`.

## Structure of repository

### /base/flux-system

Base manifest (a template) for FluxCD itself. Because we use Terraform to
bootstrap FluxCD then we use Helm releases rather than FluxCD application.
One installs FluxCD controllers, the second installs GitRepository and the
main FluxCD application (Kustomization).

In a base directory, there is a HelmRelease for FluxCD controllers only.

### /base/infrastructure

Base manifest for infrastructure applications (HelmReleases, Kustomizations,
Namespaces, etc.). Applications are split by logical parts in subdirectories
and the manifest refers to these subdirectories.

### /base/tenants

Base manifest for cluster tenants.

### /base/tenants/\_tenant

The template application for tenants. It uses variable substitution and
requires to set variables: `tenant`, `repo_url` and `environment`.

The template adds Namespace, GitRepository with a tenant's manifests,
tenant's main FluxCD application that refers to this repository and sets RBAC
to allow to use FluxCD resources.

FluxCD has multi-tenancy mode enabled and the tenant's namespace is isolated
from other namespaces.

### /ENVIRON

The entry point (path) for the main FluxCD application (Kustomization). In
the manifest, it adds all other subdirectories.

### /ENVIRON/flux-system

The manifest for full FluxCD installation: controllers with the main
application. It refers to the base manifest and can have patches.

### /ENVIRON/infrastructure

The manifest for infrastructure applications (HelmReleases, Kustomizations,
Namespaces, etc.). It refers to the base manifest and can have patches or
additional applications.

### /ENVIRON/tenants

The manifest for cluster tenants. It refers to the base manifest and can have
patches (like adding the `environment` variabel) or additional tenants.
