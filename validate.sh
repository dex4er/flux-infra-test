#!/usr/bin/env bash

# This script downloads the Flux OpenAPI schemas, then it validates the
# Flux custom resources and the kustomize overlays using kubeconform.
# This script is meant to be run locally and in CI before the changes
# are merged on the main branch that's synced by Flux.

# Copyright 2020 The Flux authors. All rights reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# This script is meant to be run locally and in CI to validate the Kubernetes
# manifests (including Flux custom resources) before changes are merged into
# the branch synced by Flux in-cluster.

# Prerequisites (run `while read -r plugin _version; do asdf plugin add "$plugin"; done < .tool-versions; asdf install`)
# - yq v4.6
# - kustomize v4.1
# - kubeconform v0.4.12

set -o errexit
set -o pipefail
shopt -s checkwinsize
/usr/bin/env true

environments=(playground)

(
  if ! [[ -d .flux-crd-schemas/master-standalone-strict ]]; then
    echo "INFO - Downloading Flux OpenAPI schemas"
    mkdir -p .flux-crd-schemas/master-standalone-strict
    curl -sL https://github.com/fluxcd/flux2/releases/latest/download/crd-schemas.tar.gz | tar zxf - -C .flux-crd-schemas/master-standalone-strict
  fi

  kubeconform_config=("-strict" "-ignore-missing-schemas" "-schema-location" "default" "-schema-location" ".flux-crd-schemas" "-verbose")

  echo "INFO - Validating environments"
  find "${environments[@]}" -maxdepth 2 -type f -name '*.yaml' -print0 | while IFS= read -r -d $'\0' file; do
    kubeconform "${kubeconform_config[@]}" "${file}"
    if [[ ${PIPESTATUS[0]} != 0 ]]; then
      exit 1
    fi
  done

  # mirror kustomize-controller build options
  kustomize_flags=("--load-restrictor=LoadRestrictionsNone")
  kustomize_config="kustomization.yaml"

  echo "INFO - Validating kustomize overlays"
  find . -type f -name "${kustomize_config}" -print0 | while IFS= read -r -d $'\0' file; do
    echo "INFO - Validating kustomization ${file/%${kustomize_config}/}"
    kustomize build "${file/%${kustomize_config}/}" "${kustomize_flags[@]}" |
      env \
        account_id=123456789012 \
        environment=validate \
        repo_url=http://gitlab.com/repo \
        tenant=tenant \
        envsubst |
      kubeconform "${kubeconform_config[@]}"
    if [[ ${PIPESTATUS[0]} != 0 ]]; then
      exit 1
    fi
    for f in "${file/%${kustomize_config}/}"*.yaml; do
      if [[ ${f##*/} != "kustomization.yaml" ]] && [[ ${f##*/} != "ephemeral-template.yaml" ]]; then
        if [[ $(yq .apiVersion "${f}" || true) == "kustomize.toolkit.fluxcd.io/v1beta2" ]]; then
          echo "INFO - Checking if path in ${f} exists"
          if ! [[ -d $(yq .spec.path "${f}" || true) ]]; then
            exit 1
          fi
        fi
      fi
    done
  done

  echo "All manifests are valid."
) | COLUMNS=${COLUMNS} perl -pae '
  BEGIN { $|=1 };
  chomp;
  $_ = substr($_, 0, ($ENV{COLUMNS}||80)-1);
  $max = length($_) > $max ? length($_) : $max;
  $_.=" " x ($max-length($_)) . "\r";
  END { print "\n" }
'
